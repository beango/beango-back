<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
    // User Routes
    Route::get('user-data', 'API\UserController@details');
    Route::get('user-list', 'API\UserController@getList');
    Route::get('user/{id}', 'API\UserController@getById');
    Route::post('user', 'API\UserController@create');
    Route::put('user/{id}', 'API\UserController@update');
    Route::put('user/{id}/block', 'API\UserController@blockUserById');
    Route::get('user-contracts/{userId}', 'API\UserController@getContracts');

    // Data Routes
    Route::get('ext-data', 'API\DataController@getData');

    Route::get('settings', 'API\DataController@getList');
    Route::put('settings', 'API\DataController@update');

    // Offer routes
    Route::post('offer', 'API\OfferController@create');
    Route::put('offer/{id}', 'API\OfferController@update');
    Route::get('offer/{id}', 'API\OfferController@getById');
    Route::get('offer-list', 'API\OfferController@getList');
    Route::put('offer/{id}/cancel', 'API\OfferController@cancel');

    // BarterOffer routes
    Route::post('barter-offer', 'API\BarterOfferController@create');
    Route::put('barter-offer/{id}', 'API\BarterOfferController@update');
    Route::get('barter-offer/{id}', 'API\BarterOfferController@getById');
    Route::get('barter-offer-list', 'API\BarterOfferController@getList');
    Route::put('barter-offer/{id}/cancel', 'API\BarterOfferController@cancel');
    Route::put('barter-offer/{id}/accept', 'API\BarterOfferController@accept');

    // Contract routes
    Route::post('contract', 'API\ContractController@create');
    Route::put('contract/{id}', 'API\ContractController@update');
    Route::get('contract/{id}', 'API\ContractController@getById');
    Route::get('contract-list', 'API\ContractController@getList');
    Route::put('contract/{id}/cancel', 'API\ContractController@cancel');

    // BarterContract routes
    Route::post('barter-contract', 'API\BarterContractController@create');
    Route::put('barter-contract/{id}', 'API\BarterContractController@update');
    Route::get('barter-contract/{id}', 'API\BarterContractController@getById');
    Route::get('barter-contract-list', 'API\BarterContractController@getList');
    Route::put('barter-contract/{id}/cancel', 'API\BarterContractController@cancel');


    //set id hash
    Route::get('offers/set-id-hash', 'API\OfferController@setIdHash');
    Route::get('contracts/set-id-hash', 'API\ContractController@setIdHash');
    Route::get('barter-offers/set-id-hash', 'API\BarterOfferController@setIdHash');
    Route::get('barter-contracts/set-id-hash', 'API\BarterContractController@setIdHash');
});