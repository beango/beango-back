<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->text('harvest')->nullable();
            $table->text('credit_terms')->nullable();
            $table->text('delivery_location')->nullable();
            $table->timestamp('delivery_period')->nullable();
            $table->dropColumn(['lot_description']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn([
                'harvest',
                'credit_terms',
                'delivery_location',
                'delivery_period'
                ]);
            $table->text('lot_description')->nullable();
        });
    }
}
