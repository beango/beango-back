<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarterOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barter_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('grower_id');
            $table->integer('barter_id');
            $table->integer('user_id');
            $table->double('total_contracts_price', 20);
            $table->double('agreed_price', 20);
            $table->enum('status', ['open', 'closed', 'cancelled']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barter_offers');
    }
}
