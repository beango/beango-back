<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnBarterOffersRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relations_contracts_barter_offers', function (Blueprint $table) {
            $table->dropColumn('offer_id');
            $table->integer('barter_offer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relations_contracts_barter_offers', function (Blueprint $table) {
            $table->dropColumn('barter_offer_id');
            $table->integer('offer_id');
        });
    }
}
