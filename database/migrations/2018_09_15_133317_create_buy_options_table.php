<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barter_offer_id')->index();
            $table->string('barter_offer_id_hash')->index();
            $table->string('coffee_type')->nullable();
            $table->text('harvest')->nullable();
            $table->text('credit_terms')->nullable();
            $table->text('processing')->nullable();
            $table->text('delivery_location')->nullable();
            $table->text('package')->nullable();
            $table->timestamp('delivery_period_from')->nullable();
            $table->timestamp('delivery_period_to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_options');
    }
}
