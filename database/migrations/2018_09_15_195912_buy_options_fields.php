<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuyOptionsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buy_options', function (Blueprint $table) {
            $table->dropColumn(['volume','unit_price']);
        });
        Schema::table('buy_options', function (Blueprint $table) {
            $table->float('volume_offered')->nullable();
            $table->float('local_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buy_options', function (Blueprint $table) {
            $table->dropColumn(['volume_offered','local_price']);
        });
        Schema::table('buy_options', function (Blueprint $table) {
            $table->float('volume')->nullable();
            $table->float('unit_price')->nullable();
        });
    }
}
