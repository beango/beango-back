<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTotalValueField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barter_offers', function (Blueprint $table) {
            $table->dropColumn(['total_value']);
        });
        Schema::table('barter_offers', function (Blueprint $table) {
            $table->float('total_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barter_offers', function (Blueprint $table) {
            $table->dropColumn(['total_price']);
        });

        Schema::table('barter_offers', function (Blueprint $table) {
            $table->float('total_value')->nullable();
        });
    }
}
