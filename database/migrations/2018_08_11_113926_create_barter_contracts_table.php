<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarterContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barter_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('barter_offer_id');
            $table->enum('status', ['approved', 'cancelled', 'pending', 'completed']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barter_contracts');
    }
}
