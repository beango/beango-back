<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('role', ['admin', 'grower', 'buyer', 'barter']);
            $table->string('company_name')->nullable();
            $table->string('farm_name')->nullable();
            $table->string('region')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('cpf')->nullable();
            $table->string('cnpf')->nullable();
            $table->string('state_registration')->nullable();
            $table->string('phone')->nullable();
            $table->string('web_site')->nullable();
            $table->text('farm_description')->nullable();
            $table->string('agent')->nullable();
            $table->float('agent_commission')->nullable();
            $table->tinyInteger('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'role',
                'contact_name',
                'company_name',
                'farm_name',
                'region',
                'address',
                'city',
                'state',
                'zip_code',
                'cpf',
                'cnpf',
                'state_registration',
                'phone',
                'web_site',
                'farm_description',
                'agent',
                'agent_commission',
                'status',
            ]);
        });
    }
}
