<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationsContractsAddContractUserId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relations_contracts_barter_offers', function (Blueprint $table) {
            $table->integer('barter_user_id')->nullable();
            $table->float('agreed_price')->nullable();
        });

        Schema::table('contracts', function (Blueprint $table) {
            $table->integer('is_blocked');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relations_contracts_barter_offers', function (Blueprint $table) {
            $table->dropColumn(['barter_user_id', 'agreed_price']);
        });

        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn(['is_blocked']);
        });
    }
}
