<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('offer_id');
            $table->integer('buyer_id');
            $table->integer('seller_id');
            $table->text('contract_description')->nullable();
            $table->float('volume')->nullable();
            $table->string('delivery_location')->nullable();
            $table->float('unit_price')->nullable();
            $table->string('certification')->nullable();
            $table->enum('status', ['pending', 'approved', 'cancelled'])->default('pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
