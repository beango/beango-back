<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddEnumValueContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function (Blueprint $table) {
            DB::statement("ALTER TABLE `contracts` CHANGE `status` `status` ENUM('pending','approved','cancelled','completed') NOT NULL  DEFAULT 'pending'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            DB::statement("ALTER TABLE `contracts` CHANGE `status` `status` ENUM('pending','approved','cancelled') NOT NULL  DEFAULT 'pending'");
        });
    }
}
