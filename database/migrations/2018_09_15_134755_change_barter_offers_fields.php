<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBarterOffersFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barter_offers', function (Blueprint $table) {
            $table->dropColumn(['grower_id', 'barter_id']);
        });
        Schema::table('barter_offers', function (Blueprint $table) {
            $table->integer('offer_owner_id')->nullable();
            $table->integer('offer_to_id')->nullable();
            $table->float('total_value')->nullable();
            $table->boolean('is_contract')->default(false);
            $table->boolean('is_buy')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barter_offers', function (Blueprint $table) {
            $table->dropColumn([
                'offer_owner_id',
                'offer_to_id',
                'total_value',
                'is_contract',
                'is_buy'
            ]);
        });
        Schema::table('barter_offers', function (Blueprint $table) {
            $table->integer('grower_id')->nullable();
            $table->integer('barter_id')->nullable();
        });
    }
}
