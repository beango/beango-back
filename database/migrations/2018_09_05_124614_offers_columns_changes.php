<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OffersColumnsChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {

            $table->text('comment')->nullable();
            $table->timestamp('delivery_period_from')->nullable();
            $table->timestamp('delivery_period_to')->nullable();
            $table->integer('offer_owner_id')->nullable();
            $table->text('package')->nullable();

            $table->dropColumn(['delivery_period', 'package_options']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn([
                'delivery_period_from',
                'delivery_period_to',
                'comment',
                'offer_owner_id',
                'package'
            ]);
            $table->timestamp('delivery_period')->nullable();
            $table->text('package_options')->nullable();
        });
    }
}
