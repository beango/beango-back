<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OffersProcessingTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn(['processing', 'delivery_period_to', 'delivery_period_from']);
        });
        Schema::table('offers', function (Blueprint $table) {
            $table->timestampTz('delivery_period_from')->nullable();
            $table->timestampTz('delivery_period_to')->nullable();
            $table->text('processing')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn(['processing', 'delivery_period_to', 'delivery_period_from']);
        });
        Schema::table('offers', function (Blueprint $table) {
            $table->timestampTz('delivery_period_from')->nullable();
            $table->timestampTz('delivery_period_to')->nullable();
            $table->text('processing')->nullable();
        });
    }
}
