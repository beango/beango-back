<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserOfferRolesOwnerTo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relations_users_offers', function (Blueprint $table) {
            $table->dropColumn(['user_role']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relations_users_offers', function (Blueprint $table) {
            $table->dropColumn(['user_role']);
        });
        Schema::table('relations_users_offers', function (Blueprint $table) {
            $table->enum('user_role', ['grower', 'buyer', 'barter']);
        });
    }
}
