<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOfferTypeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barter_offers', function (Blueprint $table) {
            $table->enum('offer_type', ['barter'])->defalut('barter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barter_offers', function (Blueprint $table) {
            $table->dropColumn(['offer_type']);
        });
    }
}
