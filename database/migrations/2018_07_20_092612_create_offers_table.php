<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id');
            $table->string('offertype');
            $table->string('coffee_type')->nullable();
            $table->string('certification')->nullable();
            $table->integer('lot_number')->nullable();
            $table->text('lot_description')->nullable();
            $table->tinyInteger('varietal')->default(0);
            $table->tinyInteger('processing')->default(0);
            $table->string('cupping_notes')->nullable();
            $table->float('local_price')->default(0);
            $table->float('local_price_unit')->default(0);
            $table->float('volume_offered')->default(0);
            $table->float('min_volume')->default(0);
            $table->json('packageoptions')->nullable();
            $table->float('volume_available')->default(0);
            $table->float('volume_agreed')->default(0);
            $table->enum('status', ['open', 'closed', 'cancelled'])->default('open');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
