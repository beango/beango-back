<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

class ChangeDoublesTotal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            DB::statement("ALTER TABLE `offers` CHANGE `local_price` `local_price` DOUBLE(20,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `offers` CHANGE `local_price_unit` `local_price_unit` DOUBLE(20,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `offers` CHANGE `volume_offered` `volume_offered` DOUBLE(20,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `offers` CHANGE `min_volume` `min_volume` DOUBLE(20,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `offers` CHANGE `volume_available` `volume_available` DOUBLE(20,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `offers` CHANGE `volume_agreed` `volume_agreed` DOUBLE(20,2)  NOT NULL  DEFAULT '0.00';");

            DB::statement("ALTER TABLE `contracts` CHANGE `volume` `volume` DOUBLE(20,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `contracts` CHANGE `unit_price` `unit_price` DOUBLE(20,2)  NOT NULL  DEFAULT '0.00';");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            DB::statement("ALTER TABLE `offers` CHANGE `local_price` `local_price` DOUBLE(8,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `offers` CHANGE `local_price_unit` `local_price_unit` DOUBLE(8,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `offers` CHANGE `volume_offered` `volume_offered` DOUBLE(8,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `offers` CHANGE `min_volume` `min_volume` DOUBLE(8,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `offers` CHANGE `volume_available` `volume_available` DOUBLE(8,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `offers` CHANGE `volume_agreed` `volume_agreed` DOUBLE(8,2)  NOT NULL  DEFAULT '0.00';");

            DB::statement("ALTER TABLE `contracts` CHANGE `volume` `volume` DOUBLE(8,2)  NOT NULL  DEFAULT '0.00';");
            DB::statement("ALTER TABLE `contracts` CHANGE `unit_price` `unit_price` DOUBLE(8,2)  NOT NULL  DEFAULT '0.00';");
        });
    }
}
