<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('package_options');
            $table->dropColumn('varietal');
        });

        Schema::table('offers', function (Blueprint $table) {
            $table->text('package_options');
            $table->text('varietal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('package_options');
            $table->dropColumn('varietal');
        });

        Schema::table('offers', function (Blueprint $table) {
            $table->text('package_options');
            $table->text('varietal');
        });
    }
}
