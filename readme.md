Environment specification:
- PHP 7+
- MySQL 5.7
- Nginx(could be changed with Apache or other server software)
- Composer(package manager for PHP)

The logic part based on the **Laravel Framework**. 
That decision was made for several reasons:
- good documentation
- big community(if you, unfortunately, didn't find the answer in documentation, there are a lot of people that can help you)
- you can start a project very fast, just install Composer and write one command in terminal)
- many important things(like user authentication, caching, ORM, etc) are ready for use from the box, you don't need to invent the wheel
- security
- speed
- beautiful database migrations

If you want to go deeper, just visit https://laravel.com 

**How to install BeanGo:**
- be sure that you have prepared an environment
- download sources
- open console and go to the sources folder 
- run 'php artisan migrate'(it will prepare database)
- configure your server(nginx/apache) to direct all requests to the /{sources}/public/index.php
- PROFIT

**Main Structure of the project:**
- .env file —— configuration of the application(parameters for connection to a database, mail config, etc)
- ./public/index.php —— the entry point of the app. All requests are going here. This file loads all other(needed) files, classes, configs, etc
- ./database/migrations/ —— All database changes are located here
- ./routes/ —— all endpoints(URL) with relations to the logic part are located here. For BeanGo we need to manipulate with ./routes/api.php file because we've created an API 
- ./app/***.php —— **Models**(Objects/Entities) with which we can do manipulation are located in these files.
- ./app/HTTP/Controller/API/ —— here is a business logic of the project. These files(**Controllers**) are making decisions(to show a list of offers or to authenticate the client or anything else) what do when the user requests specific API route(URL)

**The project has general Models:**
- Offer([read more](Offer-Model))
- Contract
- BarterOffer
- BarterContract
- User([read more](User-Model))
- Settings([read more](Settings-Model))

These Models are describing what fields we can use, what relations with other Models, how we need to preprocess data, storing some predefined constants. 

**And also it has additional Models:**
- RelationsUserOffer
- RelationsContractBarterOffer

These Models are used to connect many Users to one Offer, many Contracts to one BarterOffer. In programmers world, it is calling **relation One To Many**

In our case, all Models has their own database table.

**The project has such Controllers**:
- BarterContractController
- BarterOfferController
- ContractController
- DataController
- OfferController([read more](Offer-controller))
- UserController

Controllers manipulate with Models and make logical operations with their properties and build responses for Front-End part of BeanGo.


**How RestAPI works:**

The HTTP request has a type like GET, POST, PUT, DELETE, etc.
The Application could have only one route, for example, /contract/{id}, but it will run different logic depends on the type of request 
GET /contract/{id} —— will return info about specific contract
PUT /contract/{id} —— will update info of specific contract

