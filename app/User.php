<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const ADMIN_ROLE = 'admin';
    const BUYER_ROLE = 'buyer';
    const GROWER_ROLE = 'grower';
    const BARTER_ROLE = 'barter';

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'company_name',
        'status',
        'role',
        'farm_name',
        'region',
        'address',
        'city',
        'state',
        'zip_code',
        'cpf',
        'cnpf',
        'state_registration',
        'phone',
        'web_site',
        'farm_description',
        'agent',
        'agent_commission'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guarded = ['id'];

    public static function checkUserExists($userId)
    {
        $user = self::find($userId);

        return ($user['status'] == 1) ? true : false;
    }

    public function getCreatedAtAttribute()
    {
        return strtotime($this->attributes['created_at']) * 1000;
    }

    public function getUpdatedAtAttribute()
    {
        return strtotime($this->attributes['updated_at']) * 1000;
    }

}
