<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferImage extends Model
{
    protected $table = 'offer_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_name',
        'offer_id'
    ];

    protected $guarded = ['id'];

    public function getFileNameAttribute()
    {
        return '/images/offers/' . $this->attributes['offer_id'] . '/' . $this->attributes['file_name'];
    }
}
