<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyOption extends Model
{
    protected $table = 'buy_options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'barter_offer_id',
        'barter_offer_id_hash',
        'coffee_type',
        'processing',
        'volume_offered',
        'package',
        'local_price',
        'credit_terms',
        'delivery_location',
        'harvest',
        'delivery_period_from',
        'delivery_period_to',
        'certification'
    ];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function getDeliveryPeriodToAttribute()
    {
        $date = date('F Y', strtotime($this->attributes['delivery_period_to']));
        return $date;
    }

    public function getDeliveryPeriodFromAttribute()
    {
        $date = date('F Y', strtotime($this->attributes['delivery_period_from']));
        return $date;
    }

}
