<?php

namespace App\Http\Controllers\API;

use App\BarterContract;
use App\BarterOffer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Contract;
use Validator;

class BarterContractController extends Controller
{
    public $successStatus = 200;

    /**
     * Create new BarterContract from Offer
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barter_offer_id'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 400);
        }

        $input = $request->all();
        $offerObj = BarterOffer::where('id_hash',$input['barter_offer_id'])->first();

        if(!$offerObj) {
            return response()->json(['error'=> "There is no BarterOffer with id = " . $input['barter_offer_id']], 404);
        }

        $contract['barter_offer_id'] = $offerObj->id;
        $contract['status'] = BarterContract::STATUS_PENDING;
        $contract['id_hash'] = md5($input['barter_offer_id'] . time() . rand(1, 9999999));

        $contract = BarterContract::create($contract);

        if($contract instanceof BarterContract) {
            $contract->id_hash = 'BTCT-' . substr($contract->created_at, 5, 4) . '-' . $contract->id;
            $contract->save();

            $offerObj->status = BarterOffer::STATUS_CLOSED;
            $offerObj->save();

            $contract = BarterContract::with(['BarterOffer'])->find($contract->id);

            return response()->json($contract, $this->successStatus);
        }
    }

    /**
     * Get BarterContract By Id
     *
     * @return \Illuminate\Http\Response
     */
    public function getById($contractId)
    {
        $contract = BarterContract::with(['BarterOffer'])->where('id_hash', $contractId)->first();

        if(!$contract) {
                return response()->json(['error'=> "There is no BarterContract with id = " . $contractId], 404);
        }

        return response()->json($contract, $this->successStatus);
    }

    /**
     * @param $contractId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($contractId, Request $request)
    {
        $input = $request->all();
        $contract = BarterContract::where('id_hash', $contractId)->first();

        if(!$contract) {
            return response()->json(['error'=> "There is no Contract with id = " . $contractId], 404);
        }

        foreach($input as $field => $value) {
            $contract->$field = $value;
        }

        $contract->save();

        return response()->json($contract, $this->successStatus);
    }

    /**
     * Get BarterContract List
     *
     * requested params: limit, page, sort => {-}sortField, isOpen => true/false, isMy => true/false
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
        $input = $request->all();
        $limit = 10;
        $page = 1;

        $sortField = 'created_at';
        $sortDirection = 'desc';

        if(array_key_exists('page', $input)) {
            $page = $input['page'];
        }

        if(array_key_exists('limit', $input)) {
            $limit = $input['limit'];
        }

        $offset = ($page*$limit) - $limit;
        if($offset < 0) {
            $offset = 0;
        }

        if(array_key_exists('sort', $input)) {
            if($input['sort'][0] == '-') {
                $input['sort'] = ltrim($input['sort'], '-');
                $sortField = $input['sort'];
                $sortDirection = 'desc';
                $sortBy[$input['sort']] = 'desc';
            } else {
                $sortField = $input['sort'];
                $sortDirection = 'asc';
            }
        }

        $contracts = BarterContract::with(['BarterOffer']);

        if(array_key_exists('isOpen', $input)) {
            if($input['isOpen'] === 'true') {
                $contracts->whereIn('status', [BarterContract::STATUS_PENDING, BarterContract::STATUS_APPROVED]);
            } else {
                $contracts->whereIn('status', [BarterContract::STATUS_CANCELLED, BarterContract::STATUS_COMPLETED]);
            }
        }

        $contracts->orderBy($sortField, $sortDirection)->skip($offset)->take($limit);

        $pagination = [
            'total' => (int)$contracts->count()
        ];

        $pagination['limit'] = (int)$limit;
        $pagination['page'] = (int)$offset/$limit+1;

        $response = [
            'data' => $contracts->get(),
            'pagination' => $pagination,
            'sort' => [
                $sortField => $sortDirection
            ]
        ];

        return response()->json($response, $this->successStatus);
    }

    /**
     * @param $contractId
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($contractId)
    {
        $contract = BarterContract::where('id_hash', $contractId)->first();

        if(!$contract) {
            return response()->json(['error'=> "There is no BarterContract with id = " . $contractId], 404);
        }

        $contract->status = Contract::STATUS_CANCELLED;
        $contract->save();

        return response()->json($contract, $this->successStatus);
    }

    public function setIdHash()
    {
        $contracts = BarterContract::all();
        foreach($contracts as $contract) {
            $contract->id_hash = 'BTCT-' . substr($contract->created_at, 5, 4) . '-' . $contract->id;
            $contract->save();
        }
        die;
    }
}
