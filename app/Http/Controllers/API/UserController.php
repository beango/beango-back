<?php

namespace App\Http\Controllers\API;

use App\Contract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $response = [
                'full_name'  => $user->name,
                'email'      => $user->email,
                'role'       => $user->role,
                'access_token'  => $user->createToken('MyApp')->accessToken
            ];

            return response()->json($response, $this->successStatus);
        }
        else{
            $response = [
                'status'    => 'error',
                'message'   => 'Unauthorised',
                'data'      => []
            ];
            return response()->json($response, 401);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 400);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['status'] = 1;

        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
        return response()->json(['access_token' => $success], $this->successStatus);
    }
    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json($user, $this->successStatus);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        if($user->role != User::ADMIN_ROLE) {
            return response()->json(['error'=>'You are not an Admin'], 403);
        }

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'required',
            'password' => 'required',
            'role' => 'required',
            'company_name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 400);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['status'] = 1;

        $user = User::create($input);

        return response()->json($user, $this->successStatus);
    }

    /**
     * @param $userId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($userId, Request $request)
    {
        $user = Auth::user();
        if($user->role != User::ADMIN_ROLE) {
            return response()->json(['error'=>'You are not an Admin'], 403);
        }

        $input = $request->all();
        $user = User::find($userId);

        if(isset($input['password'])) {
            $input['password'] = bcrypt($input['password']);
        }

        unset($input['created_at']);
        unset($input['updated_at']);
        unset($input['id']);
        foreach($input as $field => $value) {
            $user->$field = $value;
        }

        $user->save();


        return response()->json($user, $this->successStatus);
    }

    /**
     *  Get list of users (only for Admin)
     *
     *  requested params: limit, page, role, email, sortBy[sort_field] => asc/desc
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $user = Auth::user();
        if($user->role != User::ADMIN_ROLE) {
            return response()->json(['error'=>'You are not an Admin'], 403);
        }

        $input = $request->all();
        $limit = 10;
        $page = 1;

        $sortField = 'id';
        $sortDirection = 'asc';

        if(array_key_exists('page', $input)) {
            $page = $input['page'];
        }

        if(array_key_exists('limit', $input)) {
            $limit = $input['limit'];
        }

        $offset = ($page*$limit) - $limit;
        if($offset < 0) {
            $offset = 0;
        }

        if(array_key_exists('sort', $input)) {
            if($input['sort'][0] == '-') {
                $input['sort'] = ltrim($input['sort'], '-');
                $sortField = $input['sort'];
                $sortDirection = 'desc';
                $sortBy[$input['sort']] = 'desc';
            } else {
                $sortField = $input['sort'];
                $sortDirection = 'asc';
            }
        }

        $users = User::select('id', 'name', 'created_at', 'email', 'company_name', 'status', 'role');

        if(isset($input['role'])) {
            $users->where('role', $input['role']);
        }

        if(isset($input['email'])) {
            $users->where('role', 'LIKE', '%' . $input['email'] . '%');
        }

        $users->orderBy($sortField, $sortDirection);

        $pagination = [
            'total' => (int)$users->count()
        ];
        $users->skip($offset)->take($limit);

        $pagination['limit'] = (int)$limit;
        $pagination['page'] = (int)$offset/$limit+1;

        $response = [
            'data' => $users->get()->toArray(),
            'pagination' => $pagination,
            'sort' => [
                $sortField => $sortDirection
            ]
        ];

        return response()->json($response, $this->successStatus);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getById($id)
    {
        $user = Auth::user();
        if($user->role != User::ADMIN_ROLE) {
            return response()->json(['error'=>'You are not an Admin'], 403);
        }

        $user = User::find($id)->toArray();

        return response()->json($user, $this->successStatus);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function blockUserById($id)
    {
        $user = Auth::user();
        if($user->role != User::ADMIN_ROLE) {
            return response()->json(['error'=>'You are not an Admin'], 403);
        }

        $user = User::find($id);
        $user->status = 0;
        $user->save();

        return response()->json(["message" => "Great Success"], $this->successStatus);
    }

    /**
     * @param $userId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getContracts($userId, Request $request)
    {
        $user = User::find($userId);
        if(!$user) {
            return response()->json(['error'=>'There is no such user'], 404);
        }
        $user = $user->toArray();

//        switch($user['role']) {
//            case User::GROWER_ROLE:
//                $fieldCondition  = 'seller_id';
//                break;
//            case User::BUYER_ROLE:
//                $fieldCondition = 'buyer_id';
//                break;
//            default:
//                $fieldCondition = 'seller_id';
//                break;
//        }

        $fieldCondition = 'seller_id';

        $contracts = Contract::where($fieldCondition, $user['id'])->whereIn('status', [Contract::STATUS_APPROVED, Contract::STATUS_PENDING])->where('is_blocked', 0)->select( '*', DB::raw('contracts.id as contract_id'), DB::raw('contracts.volume*contracts.unit_price as total_price'));

        $input = $request->all();
        if(array_key_exists('minPrice', $input)) {
            $contracts->havingRaw('total_price >= ?', [$input['minPrice']]);
        }
        $contracts = $contracts->get();

        $totalSum = 0;
        $totalVolume = 0;
        foreach($contracts as $contract) {
            $totalSum += $contract->total_price;
            $totalVolume += $contract->volume;
        }

        $data = [
            'contracts' => $contracts,
            'total_price' => $totalSum,
            'total_volume' => $totalVolume
        ];
        return response()->json($data, $this->successStatus);
    }
}
