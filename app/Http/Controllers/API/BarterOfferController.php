<?php

namespace App\Http\Controllers\API;

use App\BarterOffer;
use App\BuyOption;
use App\Contract;
use App\RelationsContractBarterOffer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class BarterOfferController extends Controller
{
    public $successStatus = 200;

    /**
     * Create Barter Offer
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $input = $request->all();
        $input['is_buy'] = (int)$input['is_buy'];
        $input['is_contract'] = (int)$input['is_contract'];
        $input['offer_owner_id'] = $input['offer_owner']['id'];
        $input['offer_to_id'] = $input['offer_to'][0]['id'];

        unset($input['offer_to']);
        unset($input['offer_owner']);

        $user = Auth::user();

        $barterOfferArr = [
            'user_id' => $user->id,
            'id_hash' => md5(rand(1, 9999999) . time() . rand(1, 9999999)),
            'offer_owner_id' => $input['offer_owner_id'],
            'offer_to_id' => $input['offer_to_id'],
            'status' => BarterOffer::STATUS_OPEN,
            'is_buy' => $input['is_buy'],
            'is_contract' => $input['is_contract'],
            'total_price' => $input['total_price'],
            'total_contracts_price' => 0,
            'agreed_price' => 0,
            'comment' => $input['comment']
        ];
        $barterOffer = BarterOffer::create($barterOfferArr);
        $barterOffer->id_hash = 'BTOF-' . substr($barterOffer->created_at, 5, 4) . '-' . $barterOffer->id;
        $barterOffer->save();

        if($input['is_buy'] > 0) {
            $buyOptions = $input['buy_options'];
            $buyOptions['delivery_period_from'] = date("Y-m-d H:i:s", strtotime($input['buy_options']['delivery_period_from']));
            $buyOptions['delivery_period_to'] = date("Y-m-d H:i:s", strtotime($input['buy_options']['delivery_period_to']));
            $buyOptions['barter_offer_id'] = $barterOffer->id;
            $buyOptions['barter_offer_id_hash'] = $barterOffer->id_hash;
            BuyOption::create($buyOptions);
        }

        if($input['is_contract'] > 0) {
            $barterOffer->contract_id = $input['contracts'][0]['contract_id'];
            $barterOffer->save();
//            if(count($contracts) < 1) {
//                return response()->json(['error' => 'You should choose at least 1 contract'], 400);
//            }
//            $contractIds = [];
//            foreach($contracts as $contract) {
//                $contractIds[] = $contract['id'];
//            }
            unset($input['contracts']);

//            $contracts = Contract::whereIn('id', $contractIds)->get();
//
//            $totalContractsPrice = 0;
//            foreach($contracts as $contract) {
//                $totalContractsPrice += $contract['volume'] * $contract['unit_price'];
//            }
//
//            if($totalContractsPrice < $input['agreed_price']) {
//                return response()->json(['error' => 'Agreed price should be less than ' . $totalContractsPrice], 400);
//            }
//
//            $barterOffer->total_contracts_price = $totalContractsPrice;
//            $barterOffer->save();
//
//            foreach($contracts as $contract) {
//                $relation = new RelationsContractBarterOffer();
//                $relation->barter_offer_id = $barterOffer->id;
//                $relation->contract_id = $contract->id;
//                $contract->is_blocked = 1;
//
//                $contract->barter_offer_id = $barterOffer->id;
//                $contract->save();
//                $relation->agreed_price = $input['agreed_price'];
//                if(isset($input['barter_comment'])) {
//                    $relation->barter_comment = $input['barter_comment'];
//                }
//                $relation->barter_user_id = $input['offer_owner_id'];
//                $relation->save();
//            }
        }

        $barterOffer = BarterOffer::with([
            'user',
            'offerOwner',
            'offerTo',
            'buyOptions',
            'contracts'
        ])->find($barterOffer->id);

        return response()->json($barterOffer, $this->successStatus);
    }


    /**
     * Update Barter Offer
     *
     * @param $offerId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($offerId, Request $request)
    {
        $input = $request->all();
        $input['is_buy'] = (int)$input['is_buy'];
        $input['is_contract'] = (int)$input['is_contract'];

        $barterOffer = BarterOffer::where('id_hash', $offerId)->first();

        $relations = RelationsContractBarterOffer::where('barter_offer_id', $offerId)->get();
        foreach($relations as $relation) {
            $oldContract = Contract::where('id', $relation->contract_id)->first();
            $oldContract->is_blocked = 0;
            $oldContract->save();
            $relation->delete();
        }

        if((int)$input['is_contract'] > 0) {

            $barterOffer->contract_id = $input['contracts'][0]['id'];
            unset($input['contracts']);
//            if(count($contracts) < 1) {
//                return response()->json(['error' => 'You should choose at least 1 contract'], 400);
//            }
//            $contractIds = [];
//            foreach($contracts as $contract) {
//                $contractIds[] = $contract['contract_id'];
//            }

//            $contracts = Contract::whereIn('id', $contractIds)->get();

            // check total price of contracts. It should be greater than agreed_price
//            $totalContractsPrice = 0;
//            foreach($contracts as $contract) {
//                $totalContractsPrice += $contract['volume'] * $contract['unit_price'];
//            }
//            if($totalContractsPrice < $input['agreed_price']) {
//                return response()->json(['error' => 'Agreed price should be less than ' . $totalContractsPrice], 400);
//            }

//            $barterOffer->total_contracts_price = $totalContractsPrice;

//            $barterOffer->agreed_price = $input['agreed_price'];


//            foreach($contracts as $contract) {
//                $relation = new RelationsContractBarterOffer();
//                $relation->barter_offer_id = $barterOffer->id;
//                $relation->contract_id = $contract->id;
//                $contract->is_blocked = 1;
//                $contract->barter_offer_id = $barterOffer->id;
//                $contract->save();
//                $relation->agreed_price = $input['agreed_price'];
//                if(isset($input['barter_comment'])) {
//                    $relation->barter_comment = $input['barter_comment'];
//                }
//                $relation->barter_user_id = $input['offer_owner_id'];
//                $relation->save();
//            }
        }

        // Barter Offer
        $barterOffer->offer_owner_id = $input['offer_owner']['id'];
        $barterOffer->offer_to_id = $input['offer_to']['id'];
        $barterOffer->status = $input['status'];
        $barterOffer->comment = $input['comment'];
        $barterOffer->is_contract = (int)$input['is_contract'];
        $barterOffer->is_buy = (int)$input['is_buy'];
        $barterOffer->total_price = $input['total_price'];


        $barterOffer->save();

        $buyOption = BuyOption::where('barter_offer_id', $barterOffer->id)->first();
        if($buyOption) {
            $buyOption->delete();
        }
        if((int)$input['is_buy'] > 0) {
            $buyOptions = $input['buy_options'];
            $buyOptions['delivery_period_from'] = date("Y-m-d H:i:s", strtotime($input['buy_options']['delivery_period_from']));
            $buyOptions['delivery_period_to'] = date("Y-m-d H:i:s", strtotime($input['buy_options']['delivery_period_to']));
            $buyOptions['barter_offer_id'] = $barterOffer->id;
            $buyOptions['barter_offer_id_hash'] = $barterOffer->id_hash;

            BuyOption::create($buyOptions);
        }

        $barterOffer = BarterOffer::with([
            'user',
            'offerOwner',
            'offerTo',
            'buyOptions',
            'contracts'
        ])->find($barterOffer->id);

        return response()->json($barterOffer, $this->successStatus);
    }

    /**
     * Get BarterOffer By Id
     *
     * @return \Illuminate\Http\Response
     */
    public function getById($offerId)
    {
        $barterOffer = BarterOffer::with([
            'user',
            'offerOwner',
            'offerTo',
            'buyOptions',
            'contracts'
        ])->where('id_hash', $offerId)->first();

        if(!$barterOffer) {
            return response()->json(['error'=> "There is no BarterOffer with id = " . $offerId], 404);
        }

        return response()->json($barterOffer, $this->successStatus);
    }

    /**
     * Get BarterOffer List
     *
     * requested params: limit, page, sort => {-}sortField, isOpen => true/false, isMy => true/false
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
        $input = $request->all();
        $limit = 10;
        $page = 1;

        $sortField = 'created_at';
        $sortDirection = 'desc';

        if(array_key_exists('page', $input)) {
            $page = $input['page'];
        }

        if(array_key_exists('limit', $input)) {
            $limit = $input['limit'];
        }

        $offset = ($page*$limit) - $limit;
        if($offset < 0) {
            $offset = 0;
        }

        if(array_key_exists('sort', $input)) {
            if($input['sort'][0] == '-') {
                $input['sort'] = ltrim($input['sort'], '-');
                $sortField = $input['sort'];
                $sortDirection = 'desc';
                $sortBy[$input['sort']] = 'desc';
            } else {
                $sortField = $input['sort'];
                $sortDirection = 'asc';
            }
        }

        $offers = BarterOffer::with([
            'user' => function($query) {
                $query->select('id', 'name', 'role');
            },
            'offerOwner',
            'offerTo',
            'buyOptions',
            'contracts'
        ]);

        if(array_key_exists('isOpen', $input)) {
            if($input['isOpen'] === 'true') {
                $offers->where('status', 'open');
            } else {
                $offers->where('status', '!=', 'open');
            }
        }

        if(array_key_exists('isMy', $input)) {
            if($input['isMy'] === 'true') {
                $user = Auth::user();
                $offers->where('user_id', $user->id);
            }
        }

        $offers->orderBy($sortField, $sortDirection);

        $pagination = [
            'total' => (int)$offers->count()
        ];
        $offers->skip($offset)->take($limit);

        $pagination['limit'] = (int)$limit;
        $pagination['page'] = (int)$offset/$limit+1;

        $response = [
            'data' => $offers->get(),
            'pagination' => $pagination,
            'sort' => [
                $sortField => $sortDirection
            ]
        ];

        return response()->json($response, $this->successStatus);
    }

    /**
     * @param $offerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($offerId)
    {
        $offer = BarterOffer::where('id_hash', $offerId)->first();

        if(!$offer) {
            return response()->json(['error'=> "There is no BarterOffer with id = " . $offerId], 404);
        }

        $offer->status = BarterOffer::STATUS_CANCELLED;
        $offer->save();

        return response()->json($offer, $this->successStatus);
    }

    public function setIdHash()
    {
        $offers = BarterOffer::all();
        foreach($offers as $offer) {
            $offer->id_hash = 'BTOF-' . substr($offer->created_at, 5, 4) . '-' . $offer->id;
            $offer->save();
        }
        die;
    }

    /**
     * @param $offerId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function accept($offerId, Request $request)
    {
        $offer = BarterOffer::where('id_hash', $offerId)->first();

        if(!$offer) {
            return response()->json(['error'=> "There is no BarterOffer with id = " . $offerId], 404);
        }

        $offer->status = BarterOffer::STATUS_CLOSED;

        $input = $request->all();

        switch($input['type'])
        {
            case 'buy':
                $contractId = $this->processBuyBarterOffer($offer);
                break;
            case 'contract':
                $contractId = $this->processContractBarterOffer($offer);
                break;
        }
        if(!isset($contractId)) {
            return response()->json(['message' => 'Something went wrong'], 500);
        }

        $offer->save();

        return response()->json(['contract_id' => $contractId], $this->successStatus);
    }

    /**
     * @param $offer
     * @return mixed|string
     */
    protected function processBuyBarterOffer($offer)
    {
        $contract = new Contract();

        $contract->volume = $offer->buyOptions->volume_offered;
        $contract->unit_price = $offer->buyOptions->local_price;
        $contract->barter_offer_id = $offer->id;
        $contract->seller_id = $offer->offer_to_id;
        $contract->buyer_id = $offer->offer_owner_id;
        $contract->status = Contract::STATUS_PENDING;
        $contract->delivery_location = $offer->buyOptions->delivery_location;
        $contract->id_hash = md5($offer['id_hash'] . time() . rand(1, 9999999));
        $contract->offer_id = 0;
        $contract->comment = '';
        $contract->is_blocked = 1;
        $contract->save();

        if($contract instanceof Contract) {
            $contract->id_hash = 'CNCT-' . substr($contract->created_at, 5, 4) . '-' . $contract->id;
            $contract->save();
            $relation = new RelationsContractBarterOffer();
            $relation->barter_offer_id = $offer->id;
            $relation->contract_id = $contract->id;

            $relation->agreed_price = $offer->agreed_price;

            $relation->barter_comment = $offer->comment;

            $relation->barter_user_id = $offer->offer_owner_id;
            $relation->save();
        }

        $offer->save();

        return $contract->id_hash;
    }

    /**
     * @param $offer
     * @return mixed
     */
    protected function processContractBarterOffer($offer)
    {
        $contract = $offer->contracts[0];

        $contract->barter_offer_id = $offer->id;
        $contract->is_blocked = 1;
        $contract->save();

        $relation = new RelationsContractBarterOffer();
        $relation->barter_offer_id = $offer->id;
        $relation->contract_id = $contract->id;
        $relation->agreed_price = $offer->agreed_price;
        $relation->barter_comment = $offer->comment;
        $relation->barter_user_id = $offer->offer_owner_id;
        $relation->save();

        return $contract->id_hash;
    }
}
