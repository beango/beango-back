<?php

namespace App\Http\Controllers\API;

use App\OfferImage;
use App\RelationsUserOffer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Offer;
use Illuminate\Support\Facades\Auth;
use Validator;

class OfferController extends Controller
{
    public $successStatus = 200;

    /**
     * Create Offer
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'local_price' => 'required|numeric',
            'coffee_type' => 'required',
            'volume_offered' => 'required|numeric',
            'min_volume' => 'required|numeric',
            'offer_type'  => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 400);
        }

        $offerOwner = [];
        $offerTo = [];

        $input = $request->all();

        if(isset($input['offer_to'])) {
            $offerTo = $input['offer_to'];
            unset($input['offer_to']);
        }

        if(isset($input['offer_owner'])) {
            $offerOwner = $input['offer_owner'];
            unset($input['offer_owner']);
        }

        if(isset($input['delivery_period_from'])) {
            $input['delivery_period_from'] = date("Y-m-d H:i:s", strtotime($input['delivery_period_from']));
        }

        if(isset($input['delivery_period_to'])) {
            $input['delivery_period_to'] = date("Y-m-d H:i:s", strtotime($input['delivery_period_to']));
        }

        $user = Auth::user();
        $input['user_id'] = $user->id;

        $input['volume_available'] = $input['volume_offered'];
        $input['id_hash'] = md5(rand(100000,99999999) . time());
        $input['offer_owner_id'] = $offerOwner['id'];

        $offer = Offer::create($input);
        if(isset($offerTo)) {
            foreach($offerTo as $user) {
                $relation = new RelationsUserOffer();
                $relation->user_id = $user['id'];
                $relation->offer_id = $offer->id;
                $relation->save();
            }
        }

//        Maybe in future Offer will have images
//
//        if($request->hasfile('image_files')) {
//            foreach($input['image_files'] as $image) {
//                $offerImage = new OfferImage();
//                $offerImage->offer_id = $offer->id;
//                $offerImage->file_name = $offer->id . '_' . rand(10000, 99999) . $image->getClientOriginalName();
//                $offerImage->save();
//
//                $image->move(public_path().'/images/' . 'offers/' . $offer->id . '/', $offerImage->file_name);
//            }
//            unset($input['image_files']);
//        }

        $offer->id_hash = 'OFFR-' . substr($offer->created_at, 5, 4) . '-' . $offer->id;
        $offer->save();
        $offer = Offer::with([
            'user' => function($query) {
                $query->select(['id', 'name', 'role']);
            },
            'offerOwner' => function($query) {
                $query->select(['id', 'name', 'company_name', 'email', 'role']);
            },
            'offerTo'
        ])->find($offer->id);

        return response()->json($offer, $this->successStatus);
    }


    /**
     * Update Offer
     *
     * @param $offerId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($offerId, Request $request)
    {
        $input = $request->all();

        if(isset($input['offer_to'])) {
            $offerTo = $input['offer_to'];
            unset($input['offer_to']);
        }

        if(isset($input['offer_owner'])) {
            $offerOwnerId = $input['offer_owner']['id'];
            unset($input['offer_owner']);
            $input['offer_owner_id'] = $offerOwnerId;
        }

        if(isset($input['user'])) {
            unset($input['user']);
        }

        if(isset($input['_method'])) {
            unset($input['_method']);
        }


        unset($input['created_at']);
        unset($input['updated_at']);

        if(isset($input['delivery_period_from'])) {
            $input['delivery_period_from'] = date("Y-m-d H:i:s", strtotime($input['delivery_period_from']));
        }

        if(isset($input['delivery_period_to'])) {
            $input['delivery_period_to'] = date("Y-m-d H:i:s", strtotime($input['delivery_period_to']));
        }

//        Maybe in future Offer will have images
//        if($request->hasfile('image_files')) {
//            foreach($input['image_files'] as $image) {
//                $offerImage = new OfferImage();
//                $offerImage->offer_id = $offerId;
//                $offerImage->file_name = $offerId . '_' . rand(10000, 99999) . $image->getClientOriginalName();
//                $offerImage->save();
//
//                $image->move(public_path().'/images/' . 'offers/' . $offerId . '/', $offerImage->file_name);
//            }
//            unset($input['image_files']);
//        }

        $offer = Offer::where('id_hash', $offerId)->first();

        if(!$offer) {
            return response()->json(['error'=> "There is no Offer with id = " . $offerId], 404);
        }

        $volumeOfferedOld = $offer->volume_offered;

        foreach($input as $field => $value) {
            $offer->$field = $value;
        }

        $offer->volume_available = $offer->volume_available + ($offer->volume_offered - $volumeOfferedOld);

        $offer->save();

        $relations = RelationsUserOffer::where('offer_id', $offer->id)->get();
        foreach($relations as $relation) {
            $relation->delete();
        }

        $userIds = [];
        if(isset($offerTo)) {
            foreach($offerTo as $user) {
                if(!array_key_exists($user['id'], $userIds)) {
                    $userIds[$user['id']] = $user['id'];
                    $relation = new RelationsUserOffer();
                    $relation->user_id = $user['id'];
                    $relation->offer_id = $offer->id;
                    $relation->save();
                }
            }
        }

        $offer = Offer::with([
            'user' => function($query) {
                $query->select(['id', 'name', 'role']);
            },
            'offerOwner' => function($query) {
                $query->select(['id', 'name', 'company_name', 'email', 'role']);
            },
            'offerTo'
        ])->where('id_hash', $offerId)->first();

        return response()->json($offer, $this->successStatus);
    }

    /**
     * Get Offer By Id
     *
     * @return \Illuminate\Http\Response
     */
    public function getById($offerId)
    {
        $offer = Offer::with([
            'user' => function($query) {
                $query->select(['id', 'name', 'role']);
            },
            'offerOwner' => function($query) {
                $query->select(['id', 'name', 'company_name', 'email', 'role']);
            },
            'offerTo'
        ])->where('id_hash', $offerId)->first();

        if(!$offer) {
            return response()->json(['error'=> "There is no Offer with id = " . $offerId], 404);
        }

        return response()->json($offer, $this->successStatus);
    }
    /**
     * Get Offer List
     *
     * requested params: limit, page, sort => {-}sortField, isOpen => true/false, isMy => true/false, offer_type
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
        $input = $request->all();
        $limit = 10;
        $page = 1;
        $offerType = Offer::TYPE_SELL;

        $sortField = 'created_at';
        $sortDirection = 'desc';

        if(array_key_exists('offer_type', $input)) {
            $offerType = $input['offer_type'];
        }

        if(array_key_exists('page', $input)) {
            $page = $input['page'];
        }

        if(array_key_exists('limit', $input)) {
            $limit = $input['limit'];
        }

        $offset = ($page*$limit) - $limit;
        if($offset < 0) {
            $offset = 0;
        }

        if(array_key_exists('sort', $input)) {
            if($input['sort'][0] == '-') {
                $input['sort'] = ltrim($input['sort'], '-');
                $sortField = $input['sort'];
                $sortDirection = 'desc';
                $sortBy[$input['sort']] = 'desc';
            } else {
                $sortField = $input['sort'];
                $sortDirection = 'asc';
            }
        }

        $offers = Offer::with([
            'user' => function($query) {
                $query->select(['id', 'name', 'role']);
            },
            'offerOwner' => function($query) {
                $query->select(['id', 'name', 'company_name', 'email', 'role']);
            },
            'offerTo'
        ])->where('offer_type', $offerType);

        if(array_key_exists('isOpen', $input)) {
            if($input['isOpen'] === 'true') {
                $offers->where('status', 'open');
            } else {
                $offers->where('status', '!=', 'open');
            }
        }

        if(array_key_exists('isMy', $input)) {
            if($input['isMy'] === 'true') {
                $user = Auth::user();
                $offers->where('user_id', $user->id);
            }
        }

        $offers->orderBy($sortField, $sortDirection);

        $pagination = [
            'total' => (int)$offers->count()
        ];
        $offers->skip($offset)->take($limit);

        $pagination['limit'] = (int)$limit;
        $pagination['page'] = (int)$offset/$limit+1;

        $response = [
            'data' => $offers->get(),
            'pagination' => $pagination,
            'sort' => [
                $sortField => $sortDirection
            ]
        ];

        return response()->json($response, $this->successStatus);
    }

    /**
     * @param $offerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($offerId)
    {
        $offer = Offer::where('id_hash', $offerId)->first();

        if(!$offer) {
            return response()->json(['error'=> "There is no Offer with id = " . $offerId], 404);
        }

        $offer->status = Offer::STATUS_CANCELLED;
        $offer->save();

        return response()->json($offer, $this->successStatus);
    }

    public function setIdHash()
    {
        $offers = Offer::all();
        foreach($offers as $offer) {
            $offer->id_hash = 'OFFR-' . substr($offer->created_at, 5, 4) . '-' . $offer->id;
            $offer->save();
        }
        die;
    }
}
