<?php

namespace App\Http\Controllers\API;

use App\BarterContract;
use App\BarterOffer;
use App\Contract;
use App\Http\Controllers\Controller;
use App\Settings;
use App\User;
use App\Offer;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;

class DataController extends Controller
{
    public $successStatus = 200;

    /**
     * get additional data for forms
     *
     * @return \Illuminate\Http\Response
     */
    public function getData()
    {
        $user = Auth::user();

        $growerUsers = User::select('id', 'name', 'company_name', 'role')->whereIn('role', [User::GROWER_ROLE])->where('status', 1)->get();
        $buyerUsers = User::select('id', 'name', 'company_name', 'role')->whereIn('role', [User::BUYER_ROLE])->where('status', 1)->get();
        $barterUsers = User::select('id', 'name', 'company_name', 'role')->whereIn('role', [User::BARTER_ROLE])->where('status', 1)->get();
//
//        switch($user->role) {
//            case User::ADMIN_ROLE:
//                $growerUsers = User::select('id', 'name', 'company_name')->whereIn('role', [User::GROWER_ROLE])->where('status', 1)->get();
//                $buyerUsers = User::select('id', 'name', 'company_name')->whereIn('role', [User::BUYER_ROLE, User::BARTER_ROLE])->where('status', 1)->get();
//                $barterUsers = User::select('id', 'name', 'company_name')->whereIn('role', [User::BARTER_ROLE])->where('status', 1)->get();
//                break;
//            case User::GROWER_ROLE:
//                $buyerUsers = User::select('id', 'name', 'company_name')->whereIn('role', [User::BUYER_ROLE, User::BARTER_ROLE])->where('status', 1)->get();
//                break;
//            case User::BUYER_ROLE:
//                $growerUsers = User::select('id', 'name', 'company_name')->whereIn('role', [User::GROWER_ROLE])->where('status', 1)->get();
//                break;
//        }

        $data = Settings::all()->toArray();

        $data = json_decode($data[0]['json_value'], true);

        if(isset($growerUsers)) {
            $data['grower_users'] = $growerUsers;
        }

        if(isset($buyerUsers)) {
            $data['buyer_users'] = $buyerUsers;
        }

        if(isset($barterUsers)) {
            $data['barter_users'] = $barterUsers;
        }

        $data['statuses'] = [
            'offer' => [
                Offer::STATUS_CANCELLED,
                Offer::STATUS_OPEN,
                Offer::STATUS_CLOSED
            ],
            'contract' => [
                Contract::STATUS_CANCELLED,
                Contract::STATUS_PENDING,
                Contract::STATUS_APPROVED,
                Contract::STATUS_COMPLETED
            ],
            'barter_offer' => [
                BarterOffer::STATUS_CANCELLED,
                BarterOffer::STATUS_OPEN,
                BarterOffer::STATUS_CLOSED
            ],
            'barter_contract' => [
                BarterContract::STATUS_CANCELLED,
                BarterContract::STATUS_PENDING,
                BarterContract::STATUS_APPROVED,
                BarterContract::STATUS_COMPLETED
            ]
        ];

        $data['types'] = [
            'offers' => [
                'buy',
                'sell',
                'barter'
            ]
        ];

        return response()->json($data, $this->successStatus);
    }

    /**
     * get list of settings
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        $data = Settings::all()->toArray();

        $data = json_decode($data[0]['json_value'], true);

        return response()->json($data, $this->successStatus);
    }

    /**
     * update settings
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        if($user->role != User::ADMIN_ROLE) {
            return response()->json(['error'=>'You are not an Admin'], 403);
        }

        $input = $request->all();
        foreach($input as $key => $value) {
            if(!is_array($value) && empty(trim($value))) {
                return response()->json(['error'=>'Setting can\'t be empty'], 400);
            }
        }

        $jsonValue = json_encode($input);
        $settings = Settings::select()->limit(1)->get();

        $settings[0]->json_value = $jsonValue;

        $settings[0]->save();

        return response()->json($input, $this->successStatus);
    }
}
