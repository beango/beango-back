<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Offer;
use App\Contract;
use Validator;

class ContractController extends Controller
{
    public $successStatus = 200;

    /**
     * Create new Contract from Offer
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offer_id'  => 'required',
            'volume'    => 'required',
            'acceptor_id' => 'required',
            'offer_owner_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 400);
        }

        $input = $request->all();
        $offerObj = Offer::where('id_hash', $input['offer_id'])->first();

        if($offerObj) {
            $offer = $offerObj->toArray();
        } else {
            return response()->json(['error'=> "There is no Offer with id = " . $input['offer_id']], 404);
        }

        if($offer['min_volume'] > $input['volume']) {
            return response()->json(['error'=> "Volume should be greater than " . $offer['min_volume']], 400);
        }

        if($offer['volume_available'] < $input['volume']) {
            return response()->json(['error'=> "Volume should be less than " . $offer['volume_available']], 400);
        }

        if(!User::checkUserExists($input['acceptor_id'])) {
            return response()->json(['error'=> "There is no User with id = " . $input['acceptor_id']], 400);
        }

        if(!User::checkUserExists($input['offer_owner_id'])) {
            return response()->json(['error'=> "There is no User with id = " . $input['offer_owner_id']], 400);
        }

        if($offer['offer_type'] == Offer::TYPE_SELL) {
            $buyerId = $input['acceptor_id'];
            $sellerId = $input['offer_owner_id'];
        } else {
            $buyerId = $input['offer_owner_id'];
            $sellerId = $input['acceptor_id'];
        }

        $contract['offer_id'] = $offer['id'];
        $contract['unit_price'] = $offer['local_price'];
        $contract['volume'] = $input['volume'];
        $contract['buyer_id'] = $buyerId;
        $contract['seller_id'] = $sellerId;
        $contract['comment'] = '';
        $contract['certification'] = $offer['certification'];
        $contract['status'] = Contract::STATUS_PENDING;
        $contract['is_blocked'] = 0;

        $contract['id_hash'] = md5($offer['id_hash'] . time() . rand(1, 9999999));

        $contract = Contract::create($contract);

        if($contract instanceof Contract) {
            $contract->id_hash = 'CNCT-' . substr($contract->created_at, 5, 4) . '-' . $contract->id;
            $contract->save();

            $offerObj->volume_agreed = $offerObj->volume_agreed + $contract->volume;
            $offerObj->volume_available = $offerObj->volume_available - $contract->volume;
            $offerObj->save();

            $contract = Contract::with([
                'buyer' => function($query) {
                    return $query->select('id', 'email', 'company_name', 'role');
                },
                'seller' => function($query) {
                    return $query->select('id', 'email', 'company_name', 'role');
                },
                'offer' => function($query) {
                    return $query->select();
                },
                'barterOffer',
                'blockedBy'
            ])->find($contract->id);

            return response()->json($contract, $this->successStatus);
        }
    }

    /**
     * Get Contract By Id
     *
     * @return \Illuminate\Http\Response
     */
    public function getById($contractId)
    {
        $contract = Contract::with([
            'buyer' => function($query) {
                return $query->select('id', 'email', 'company_name', 'role');
            },
            'seller' => function($query) {
                return $query->select('id', 'email', 'company_name', 'role');
            },
            'offer' => function($query) {
                return $query->select();
            },
            'barterOffer',
            'blockedBy'
            ]
        )->where('id_hash', $contractId)->first();

        if(!$contract) {
                return response()->json(['error'=> "There is no Contract with id = " . $contractId], 404);
        }

        if(empty($contract->offer)) {
            $buyOptions = $contract->barterOffer->buyOptions->toArray();
            $buyOptions['id_hash'] = $buyOptions['barter_offer_id_hash'];
            unset($buyOptions['id']);
            unset($buyOptions['barter_offer_id']);
            unset($buyOptions['barter_offer_id_hash']);
            $buyOptions['offer_type'] = 'barter';
            $contract = $contract->toArray();
            $contract['offer'] = $buyOptions;
        }

        return response()->json($contract, $this->successStatus);
    }

    /**
     * @param $contractId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($contractId, Request $request)
    {
        $input = $request->all();
        $contract = Contract::where('id_hash', $contractId)->first();

        if(!$contract) {
            return response()->json(['error'=> "There is no Contract with id = " . $contractId], 404);
        }

        unset($input['is_blocked']);
        unset($input['offer']);
        foreach($input as $field => $value) {
            $contract->$field = $value;
        }

        if($input['comment'] == '') {
            $contract->comment = '';
        }

        $contract->save();

        return response()->json($contract, $this->successStatus);
    }

    /**
     * Get Contract List
     *
     * requested params: limit, page, sort => {-}sortField, isOpen => true/false, isMy => true/false
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
        $input = $request->all();
        $limit = 10;
        $page = 1;

        $sortField = 'created_at';
        $sortDirection = 'desc';

        if(array_key_exists('page', $input)) {
            $page = $input['page'];
        }

        if(array_key_exists('limit', $input)) {
            $limit = $input['limit'];
        }

        $offset = ($page*$limit) - $limit;
        if($offset < 0) {
            $offset = 0;
        }

        if(array_key_exists('sort', $input)) {
            if($input['sort'][0] == '-') {
                $input['sort'] = ltrim($input['sort'], '-');
                $sortField = $input['sort'];
                $sortDirection = 'desc';
                $sortBy[$input['sort']] = 'desc';
            } else {
                $sortField = $input['sort'];
                $sortDirection = 'asc';
            }
        }

        $contracts = Contract::with([
            'buyer' => function($query) {
                return $query->select('id', 'email', 'company_name', 'role');
            },
            'seller' => function($query) {
                return $query->select('id', 'email', 'company_name', 'role');
            },
            'offer' => function($query) {
                return $query->select();
            },
            'blockedBy',
            'barterOffer'
        ]);

        if(array_key_exists('isOpen', $input)) {
            if($input['isOpen'] === 'true') {
                $contracts->whereIn('status', [Contract::STATUS_PENDING, Contract::STATUS_APPROVED]);
            } else {
                $contracts->whereIn('status', [Contract::STATUS_CANCELLED, Contract::STATUS_COMPLETED]);
            }
        }

        $contracts->orderBy($sortField, $sortDirection)->skip($offset)->take($limit);

        $pagination = [
            'total' => (int)$contracts->count()
        ];

        $pagination['limit'] = (int)$limit;
        $pagination['page'] = (int)$offset/$limit+1;

        $contracts = $contracts->get();
        $contractsArr = [];
        foreach($contracts as $contract) {
            if(empty($contract->offer)) {
                $buyOptions = $contract->barterOffer->buyOptions->toArray();
                $buyOptions['id_hash'] = $buyOptions['barter_offer_id_hash'];
                $buyOptions['offer_type'] = 'barter';
                unset($buyOptions['id']);
                unset($buyOptions['barter_offer_id']);
                unset($buyOptions['barter_offer_id_hash']);
                $contract = $contract->toArray();
                $contract['offer'] = $buyOptions;
                $contractsArr[] = $contract;
            } else {
                $contract = $contract->toArray();
                $contractsArr[] = $contract;
            }
        }
        $response = [
            'data' => $contractsArr,
            'pagination' => $pagination,
            'sort' => [
                $sortField => $sortDirection
            ]
        ];

        return response()->json($response, $this->successStatus);
    }

    /**
     * @param $contractId
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($contractId)
    {
        $contract = Contract::where('id_hash', $contractId)->first();

        if(!$contract) {
            return response()->json(['error'=> "There is no Contract with id = " . $contractId], 404);
        }

        $contract->status = Contract::STATUS_CANCELLED;
        $contract->save();

        return response()->json($contract, $this->successStatus);
    }

    public function setIdHash()
    {
        $contracts = Contract::all();
        foreach($contracts as $contract) {
            $contract->id_hash = 'CNCT-' . substr($contract->created_at, 5, 4) . '-' . $contract->id;
            $contract->save();
        }
        die;
    }
}
