<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $table = 'contracts';

    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_COMPLETED = 'completed';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =[
        'offer_id',
        'seller_id',
        'buyer_id',
        'comment',
        'volume',
        'delivery_location',
        'unit_price',
        'certification',
        'status',
        'id_hash',
        'is_blocked'
    ];

    protected $guarded = ['id'];

    public function offer()
    {
        return $this->hasOne('App\Offer', 'id', 'offer_id');
    }

    public function barterOffer()
    {
        return $this->hasOne('App\BarterOffer', 'id', 'barter_offer_id');
    }

    public function buyer()
    {
        return $this->hasOne('App\User', 'id', 'buyer_id');
    }

    public function seller()
    {
        return $this->hasOne('App\User', 'id', 'seller_id');
    }

    public function blockedBy()
    {
        return $this->hasManyThrough('App\User', 'App\RelationsContractBarterOffer', 'contract_id', 'id', 'id', 'barter_user_id')->select('users.id', 'users.name', 'users.company_name', 'users.role', 'relations_contracts_barter_offers.agreed_price as blocked_value', 'relations_contracts_barter_offers.barter_comment');
    }

    public function getCreatedAtAttribute()
    {
        return strtotime($this->attributes['created_at']) * 1000;
    }

    public function getUpdatedAtAttribute()
    {
        return strtotime($this->attributes['updated_at']) * 1000;
    }

}
