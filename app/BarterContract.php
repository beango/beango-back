<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BarterContract extends Model
{
    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_COMPLETED = 'completed';

    protected $table = 'barter_contracts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'barter_offer_id',
        'status',
        'id_hash'
    ];

    protected $guarded = ['id'];

    public function barterOffer()
    {
        return $this->hasOne('App\BarterOffer', 'id', 'barter_offer_id')->with(['barter', 'grower', 'contracts']);
    }

    public function getCreatedAtAttribute()
    {
        return strtotime($this->attributes['created_at']) * 1000;
    }

    public function getUpdatedAtAttribute()
    {
        return strtotime($this->attributes['updated_at']) * 1000;
    }
}
