<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BarterOffer extends Model
{
    const STATUS_OPEN = 'open';
    const STATUS_CLOSED = 'closed';
    const STATUS_CANCELLED = 'cancelled';

    protected $table = 'barter_offers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'offer_owner_id',
        'offer_to_id',
        'status',
        'total_contracts_price',
        'agreed_price',
        'id_hash',
        'total_price',
        'is_contract',
        'is_buy',
        'comment',
        'offer_type'
    ];

    protected $guarded = ['id'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->select('id', 'name', 'email', 'company_name');
    }

    public function offerOwner()
    {
        return $this->hasOne('App\User', 'id', 'offer_owner_id')->select('id', 'name', 'email', 'company_name');
    }

    public function OfferTo()
    {
        return $this->hasMany('App\User', 'id', 'offer_to_id')->select('id', 'name', 'email', 'company_name');
    }

    public function contracts()
    {
        return $this->hasMany('App\Contract', 'id', 'contract_id')->select( '*', DB::raw('contracts.volume*contracts.unit_price as total_price'));;
    }

    public function buyOptions()
    {
        return $this->hasOne('App\BuyOption', 'barter_offer_id', 'id');
    }

    public function getCreatedAtAttribute()
    {
        return strtotime($this->attributes['created_at']) * 1000;
    }

    public function getUpdatedAtAttribute()
    {
        return strtotime($this->attributes['updated_at']) * 1000;
    }

    public function getIsContractAttribute()
    {
        return (boolean)$this->attributes['is_contract'];
    }

    public function getIsBuyAttribute()
    {
        return (boolean)$this->attributes['is_buy'];
    }
}
