<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class RelationsContractBarterOffer extends Model
{

    protected $table = 'relations_contracts_barter_offers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contract_id', 'barter_offer_id', 'barter_user_id', 'agreed_price'
    ];

    public function contract()
    {
        return $this->hasOne('App\Contract', 'id', 'contract_id');
    }

    public function offer()
    {
        return $this->hasOne('App\BarterOffer', 'id', 'barter_offer_id');
    }
}
