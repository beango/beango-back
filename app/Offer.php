<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    const TYPE_BUY = 'buy';
    const TYPE_SELL = 'sell';
    const TYPE_BARTER = 'barter';

    const STATUS_OPEN = 'open';
    const STATUS_CLOSED = 'closed';
    const STATUS_CANCELLED = 'cancelled';

    protected $table = 'offers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'offer_type',
        'coffee_type',
        'certification',
        'lot_number',
        'processing',
        'cupping_notes',
        'local_price',
        'local_price_unit',
        'volume_offered',
        'min_volume',
        'package',
        'volume_available',
        'volume_agreed',
        'status',
        'title',
        'id_hash',
        'credit_terms',
        'delivery_location',
        'harvest',
        'comment',
        'delivery_period_from',
        'delivery_period_to',
        'offer_owner_id'
    ];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function offerTo()
    {
        return $this->hasManyThrough('App\User', 'App\RelationsUserOffer', 'offer_id', 'id', 'id', 'user_id')->select('users.id', 'users.name', 'users.company_name', 'users.role');
    }

    public function offerOwner()
    {
        return $this->hasOne('App\User', 'id', 'offer_owner_id');
    }

    public function getCreatedAtAttribute()
    {
        return strtotime($this->attributes['created_at']) * 1000;
    }

    public function getUpdatedAtAttribute()
    {
        return strtotime($this->attributes['updated_at']) * 1000;
    }

    public function getDeliveryPeriodToAttribute()
    {
        $date = date('F Y', strtotime($this->attributes['delivery_period_to']));
        return $date;
    }

    public function getDeliveryPeriodFromAttribute()
    {
        $date = date('F Y', strtotime($this->attributes['delivery_period_from']));
        return $date;
    }

    public function images()
    {
        return $this->hasMany('App\OfferImage', 'offer_id', 'id')->select('offer_images.file_name', 'offer_images.offer_id');
    }
}
