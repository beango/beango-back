<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class RelationsUserOffer extends Model
{

    protected $table = 'relations_users_offers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'offer_id'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function offer()
    {
        return $this->hasOne('App\Offer', 'id', 'offer_id');
    }
}
